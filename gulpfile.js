'use strict'
const gulp = require('gulp');
const sass = require('gulp-sass');
const browserSync = require('browser-sync').create();
const del = require('del'); 
const imagemin = require('gulp-imagemin');
const uglify = require('gulp-uglify');
const rev = require('gulp-rev');
const cleanCss = require('gulp-clean-css');
const flatmap = require('gulp-flatmap');
const htmlmin = require('gulp-htmlmin');

gulp.task('sass', function(){
   return gulp.src('./css/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./css'))
        .pipe(browserSync.stream());
});

gulp.task('sass:watch', function(){
    return gulp.watch('./css/*.scss',['sass']);
});

gulp.task('browaser-sync',function(){
    const files =['./*.html','./css/*.css','./img/*.{png,jpg,gif}','./js/*.js'];
    browserSync.init(files,{
        server:{
            baseDir:'./'
        }
     }); 

});
gulp.task('default',['browaser-sync'], function(){
    gulp.start('sass:watch');
});
gulp.task('clean',function(){
    return del(['dist']);
});
gulp.task('copyfonts',function(){
    gulp.src('./node_modules/open-iconic/font/*.{ttf,wpff,eof,svg,eot,otf}')
    .pipe(gulp.dest('./dist/fonts')); 
});
gulp.task('imagemin',function(){
    return gulp.src('./images/*.{png,jpg,jpeg,gif}')
    .pipe(imagemin({ optimizationLevel: 3, progressive: true, interlaced:true}))
    .pipe(gulp.dest('dist/images')); 
});

gulp.task('usemin',function(){
    return gulp.src('./*.html')
    .pipe(flatmap(function(stream, file){
        return stream.pipe(usemin({
            css:[rev()],
            html: [function(){return htmlmin({collapseWhitespace:true})}],
            js:[ugligy(),rev()],
            inlinejs: [uglify()],
            inlinecss:[cleanCss(), 'concat']
        }));
    })).pipe(gulp.dest('dist/')); 
});
gulp.task('build',['clean'],function(){
    gulp.start('copyfonts','imagemin','usemin'); 
}); 